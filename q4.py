from sklearn.metrics import accuracy_score, f1_score

from q1 import load_dataset, data_cleaning
from q3 import decision_tree, naive_bayes_gaussian, naive_bayes_bernoulli, random_forest, SVM


def get_accuracy_score(y_test, y_predict):
    accuracy = accuracy_score(y_test, y_predict)
    return accuracy


def get_f1_score(y_test, y_predict):
    f1 = f1_score(y_test, y_predict)
    return f1


if __name__ == '__main__':
    data_set = load_dataset('fout.csv')
    cleaned_dataset = data_cleaning(data_set)
    decision_tree_y_test, decision_tree_y_predict = decision_tree(cleaned_dataset)
    nb_gaussian_y_test, nb_gaussian_y_predict = naive_bayes_gaussian(cleaned_dataset)
    nb_bernoulli_y_test, nb_bernoulli_y_predict = naive_bayes_bernoulli(cleaned_dataset)
    random_forest_y_test, random_forest_y_predict = random_forest(cleaned_dataset)
    SVM_y_test, SVM_y_predict = SVM(cleaned_dataset)

    decision_tree_accuracy = get_accuracy_score(decision_tree_y_test, decision_tree_y_predict)
    print("Decision Tree Accuracy: ", decision_tree_accuracy)

    nb_gaussian_accuracy = get_accuracy_score(nb_gaussian_y_test, nb_gaussian_y_predict)
    print("Naive Bayes Gaussian Accuracy: ", nb_gaussian_accuracy)

    nb_bernoulli_accuracy = get_accuracy_score(nb_bernoulli_y_test, nb_bernoulli_y_predict)
    print("Naive Bayes Bernoulli Accuracy: ", nb_bernoulli_accuracy)

    random_forest_accuracy = get_accuracy_score(random_forest_y_test, random_forest_y_predict)
    print("Random Forest Accuracy: ", random_forest_accuracy)

    SVM_accuracy = get_accuracy_score(SVM_y_test, SVM_y_predict)
    print("SVM Accuracy: ", SVM_accuracy)

    print("-----------------------------------")

    decision_tree_f1 = get_f1_score(decision_tree_y_test, decision_tree_y_predict)
    print("Decision Tree Score: ", decision_tree_f1)

    nb_gaussian_f1 = get_f1_score(nb_gaussian_y_test, nb_gaussian_y_predict)
    print("Naive Bayes Gaussian Score: ", nb_gaussian_f1)

    nb_bernoulli_f1 = get_f1_score(nb_bernoulli_y_test, nb_bernoulli_y_predict)
    print("Naive Bayes Bernoulli Score: ", nb_bernoulli_f1)

    random_forest_f1 = get_f1_score(random_forest_y_test, random_forest_y_predict)
    print("Random Forest Score: ", random_forest_f1)

    SVM_f1 = get_f1_score(SVM_y_test, SVM_y_predict)
    print("SVM Score: ", SVM_f1)
