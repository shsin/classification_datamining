from math import sqrt

from sklearn.metrics import accuracy_score

from q1 import load_dataset, data_cleaning
from sklearn.tree import DecisionTreeClassifier, export_graphviz
import os
from sklearn.naive_bayes import GaussianNB, BernoulliNB
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm, metrics


def get_train_and_test_sets(dataset):
    target_column = 'wealth'
    # y contains the target variable
    y = dataset[target_column]

    feature_columns = list(dataset.columns)
    feature_columns.remove(target_column)
    # X contains the feature variables
    X = dataset[feature_columns]
    xTrain, xTest, yTrain, yTest = train_test_split(X, y, test_size=0.2, random_state=0)

    return xTrain, xTest, yTrain, yTest


def decision_tree(dataset, max_depth=6):
    xTrain, xTest, yTrain, yTest = get_train_and_test_sets(dataset)

    classifier = DecisionTreeClassifier(max_depth=max_depth)
    classifier.fit(xTrain, yTrain)

    target_column = 'wealth'
    feature_columns = list(dataset.columns)
    feature_columns.remove(target_column)
    export_graphviz(
        classifier,
        out_file='decision_tree.dot',
        feature_names=feature_columns,
        class_names=target_column,
        rounded=True,
        filled=True
    )
    # convert .odt file to .png
    os.system('dot -Tpng decision_tree.dot -o decision_tree.png')

    yPredict = classifier.predict(xTest)

    return yTest, yPredict


def naive_bayes_gaussian(dataset):
    xTrain, xTest, yTrain, yTest = get_train_and_test_sets(dataset)

    # Create a Gaussian Classifier
    model = GaussianNB()

    # Train the model using the training sets
    model.fit(xTrain, yTrain)

    # Predict Output
    yPredict = model.predict(xTest)

    return yTest, yPredict


def naive_bayes_bernoulli(dataset):
    xTrain, xTest, yTrain, yTest = get_train_and_test_sets(dataset)

    # Create a Gaussian Classifier
    model = BernoulliNB()

    # Train the model using the training sets
    model.fit(xTrain, yTrain)

    # Predict Output
    yPredict = model.predict(xTest)

    return yTest, yPredict


def random_forest(dataset):
    xTrain, xTest, yTrain, yTest = get_train_and_test_sets(dataset)

    regressor = RandomForestClassifier()

    regressor.fit(xTrain, yTrain)

    yPredict = regressor.predict(xTest)

    return yTest, yPredict


def SVM(dataset):
    xTrain, xTest, yTrain, yTest = get_train_and_test_sets(dataset)

    # Create a svm Classifier
    classifier = svm.SVC(kernel='linear')  # Linear Kernel

    # Train the model using the training sets
    classifier.fit(xTrain, yTrain)

    # Predict the response for test dataset
    yPredict = classifier.predict(xTest)

    return yTest, yPredict


if __name__ == '__main__':
    data_set = load_dataset('fout.csv')
    cleaned_dataset = data_cleaning(data_set)
    decision_tree(cleaned_dataset)
    naive_bayes_gaussian(cleaned_dataset)
    naive_bayes_bernoulli(cleaned_dataset)
    random_forest(cleaned_dataset)
    SVM(cleaned_dataset)
