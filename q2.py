import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from q1 import load_dataset, data_cleaning


def numericalplot(dataset):
    dic = dict()
    dic_2 = dict()
    for attr in dataset.columns:
        gp_s = dataset.groupby(attr).groups.keys()
        dic[attr] = list()
        for gp in gp_s:
            w_1=len(dataset[dataset["wealth"] == 1][dataset[dataset["wealth"] == 1][attr] == gp])
            w_0=len(dataset[dataset["wealth"] == 0][dataset[dataset["wealth"] == 0][attr] == gp])
            dic_2[gp] = [w_0, w_1]
        dic[attr] = dic_2.copy()
        dic_2.clear()

    for key, value in dic.items():
        rows = []
        for k,v in value.items():
            rows.append([k, v[0], 0])
            rows.append([k, v[1],1])
        d_helper = pd.DataFrame(rows , columns=['sorts','count','wealth'])
        plot = sns.catplot(x="wealth", y="count", hue="sorts", kind='bar', data=d_helper)
        plot.savefig("plots/"+"bar_"+ key)
        plot = sns.catplot(x="wealth", y="count", hue="sorts",kind="strip",data=d_helper)
        plot.savefig("plots/"+"scatter_"+ key)
    corr = dataset.corr()
    print(corr["wealth"].sort_values(ascending=False))

# def nominalplot(dataset, nominals):
#     index = 0
#     i = 0
#     g = []
#     for attr in nominals:
#         if i == 0:
#             f, axes = plt.subplots(1, 2)
#
#         g.append(sns.heatmap(pd.crosstab(dataset[attr], dataset["wealth"]), ax=axes.flat[i]))
#         g[i].set_ylabel(attr)
#         g[i].set_xlabel("wealth")
#         index = index + 1
#         #         if index == 8:
#         #             break
#         if i == 1:
#             f.tight_layout()
#             f.savefig(str(index) + '.png')
#             i = 0
#         else:
#             i = i + 1
#
#         # sns.heatmap(x=attr, y ="wealth", data=dataset, ax= axes[index])


if __name__ == '__main__':
    nominals = ["workclass", "education", "marital_status", "post", "relationship", "nation", "gender", "country",
                "wealth"]
    numericals = ["education_num", "age", "hours_per_week", "fnlwgt", "capital_gain", "capital_loss"]

    dataset = load_dataset('fout.csv')
    cleaned_dataset = data_cleaning(dataset)

    numericalplot(cleaned_dataset)

    # In[17]:

    #nominalplot(cleaned_dataset, nominals)
