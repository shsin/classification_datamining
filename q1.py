import numpy as np
import pandas as pd
from sklearn import preprocessing


def load_dataset(path="fout.csv"):
    return pd.read_csv(path,
                       names=['age', 'workclass', 'fnlwgt', 'education', 'education_num', 'marital_status', 'post',
                              'relationship', 'nation', 'gender', 'capital_gain', 'capital_loss', 'hours_per_week',
                              'country', 'wealth'])


def handle_outliers(dataset):
    age = dataset["age"].copy()
    hpw = dataset["hours_per_week"].copy()
    c_gain = dataset["capital_gain"].copy()
    c_loss = dataset["capital_loss"].copy()
    dataset = dataset.drop(columns=["age", "hours_per_week", "capital_gain", "capital_loss"])
    Q1 = dataset.quantile(0.25)
    Q3 = dataset.quantile(0.75)
    IQR = Q3 - Q1
    # print(IQR)
    # result = (dataset < (Q1 - 1.5 * IQR)) |(dataset > (Q3 + 1.5 * IQR))
    dataset["age"] = age
    dataset["hours_per_week"] = hpw
    dataset["capital_gain"] = c_gain
    dataset["capital_loss"] = c_loss
    # age ke capital_gain va capital_loss add shodan bayad az to iqr hazf shan
    result = dataset[~((dataset < (Q1 - 1.5 * IQR)) | (dataset > (Q3 + 1.5 * IQR))).any(axis=1)]
    return result


def fill(dataset):
    dataset.replace(' ?', np.nan, inplace=True)
    dataset = dataset.dropna()
    return dataset


def change_nominal_to_numerical(dataset, nominals):
        # encoder = preprocessing.LabelBinarizer()
        for nominal in nominals:
            # print(nominal)
            # print(dataset[nominal].tolist()[0])
            if type(dataset[nominal].tolist()[0]) != str:
                continue
            if nominal == "workclass":
                encoder = preprocessing.LabelEncoder()
                nominal_ = encoder.fit_transform(dataset[nominal])
                # print(encoder.classes_)
                dataset = dataset.drop(columns=[nominal])
                dataset[nominal] = nominal_.tolist()
                # dataset = dataset.join(pd.DataFrame(nominal_,
                #                                     columns=encoder.classes_,
                #                                     index=dataset.index))

            if nominal == "education":
                new_list = list()
                index = 0
                for x in dataset["education"]:
                    if x.strip() in ["9th", "10th", "11th", "12th"]:
                        new_list.append("9to12th")
                    else:
                        new_list.append(x)
                    index = index + 1

                dataset = dataset.drop(columns=["education"])
                # print(new_list)
                dataset["education"] = new_list
                encoder = preprocessing.LabelEncoder()
                nominal_ = encoder.fit_transform(dataset[nominal])
                # print(encoder.classes_)
                dataset = dataset.drop(columns=[nominal])
                dataset[nominal] = nominal_.tolist()
                # dataset = dataset.join(pd.DataFrame(nominal_,
                #                                     columns=encoder.classes_,
                #                                     index=dataset.index))

            if nominal == "marital_status":
                new_list = list()
                index = 0
                for x in dataset["marital_status"]:
                    if x.strip() in ["Widowed", "Separated"]:
                        new_list.append("Widow/Seperated")
                    elif x.strip() in ["Married-spouse-absent", "Married-AF-spouse"]:
                        new_list.append("Married-spouse-absent/AF-spouse")
                    else:
                        new_list.append(x)
                    index = index + 1
                dataset = dataset.drop(columns=["marital_status"])
                dataset["marital_status"] = new_list
                encoder = preprocessing.LabelEncoder()
                nominal_ = encoder.fit_transform(dataset[nominal])
                # print(encoder.classes_)
                dataset = dataset.drop(columns=[nominal])
                dataset[nominal] = nominal_.tolist()
                # dataset = dataset.join(pd.DataFrame(nominal_,
                #                                     columns=encoder.classes_,
                #                                     index=dataset.index))

            if nominal.strip() == "post":
                encoder = preprocessing.LabelEncoder()
                nominal_ = encoder.fit_transform(dataset[nominal])
                # print(encoder.classes_)
                dataset = dataset.drop(columns=[nominal])
                dataset[nominal] = nominal_.tolist()
                # dataset = dataset.join(pd.DataFrame(nominal_,
                #                                     columns=encoder.classes_,
                #                                     index=dataset.index))

            if nominal.strip() == "relationship":
                # print("in_relationship")
                encoder = preprocessing.LabelEncoder()
                nominal_ = encoder.fit_transform(dataset[nominal])
                # print(encoder.classes_)
                dataset = dataset.drop(columns=[nominal])
                dataset[nominal] = nominal_.tolist()
                # dataset = dataset.join(pd.DataFrame(nominal_,
                #                                     columns=encoder.classes_,
                #                                     index=dataset.index))

            if nominal.strip() == "nation":
                print("nation")
                new_list = list()
                index = 0
                for x in dataset["nation"]:
                    if x.strip() in ["Amer-Indian-Eskimo", "Asian-Pac-Islander", "Other", "Black"]:
                        new_list.append("Other")
                    else:
                        new_list.append(x)
                    index = index + 1
                dataset = dataset.drop(columns=["nation"])
                dataset["nation"] = new_list
                print(pd.factorize(dataset[nominal]))
                dataset[nominal] = pd.factorize(dataset[nominal])[0]

            if nominal.strip() == "country":
                print("country")
                new_list = list()
                index = 0
                for x in dataset["country"]:
                    if x.strip() == "United-States":
                        new_list.append(x)
                    else:
                        new_list.append("Other")
                    index = index + 1
                dataset = dataset.drop(columns=["country"])
                dataset["country"] = new_list
                print(pd.factorize(dataset[nominal]))

                dataset[nominal] = pd.factorize(dataset[nominal])[0]

            if nominal.strip() in ["gender", "wealth"]:
                print(nominal)
                print(pd.factorize(dataset[nominal]))
                dataset[nominal] = pd.factorize(dataset[nominal])[0]

            print(nominal)
            print(encoder.classes_)
        return dataset


def data_cleaning(dataset):
    dataset = dataset.drop("fnlwgt", axis=1)
    # shayad in do ta ziri hazf nasahn
    helper = []
    # for x in dataset["capital_gain"]:
    #     if x !=0 :
    #         helper.append(1)
    #     else:
    #         helper.append(0)
    # dataset["capital_gain"] = helper
    # helper = []
    # for x in dataset["capital_loss"]:
    #     if x != 0 :
    #         helper.append(1)
    #     else:
    #         helper.append(0)
    # dataset["capital_loss"] = helper
    mn_scalar = preprocessing.MinMaxScaler()
    dataset["capital_gain"] = mn_scalar.fit_transform(dataset["capital_gain"].to_numpy().reshape(-1,1))
    dataset["capital_loss"] = mn_scalar.fit_transform(dataset["capital_loss"].to_numpy().reshape(-1,1))

    # dataset = dataset.drop("capital_gain", axis=1)
    # dataset = dataset.drop("capital_loss", axis=1)
    dataset = handle_outliers(dataset)
    # print(dataset.head())
    dataset = fill(dataset)
    # print(dataset.head())
    nominals = ["workclass", "education", "marital_status", "post", "relationship", "nation", "gender", "country",
                "wealth"]
    dataset = change_nominal_to_numerical(dataset, nominals)
    return dataset


if __name__ == '__main__':
    data_set = load_dataset()
    data_set.head()
    data_set.describe()
    cleaned_dataset = data_cleaning(data_set)
    # print(cleaned_dataset["capital_gain"][0:20])
    # print(cleaned_dataset.head())
    # cleaned_dataset.describe()
    # cleaned_dataset.info()
